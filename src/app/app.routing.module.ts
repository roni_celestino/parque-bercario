import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, CanActivateChild, CanDeactivate } from '@angular/router';

// MODULOS
import { SiteModule } from './site/site.module';

const appRoutes: Routes = [
  { path: '', redirectTo: 'site', pathMatch: 'full' },
  { path: 'site', loadChildren: 'app/site/site.module#SiteModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]

})

export class AppRoutingModule { }

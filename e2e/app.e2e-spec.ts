import { ParquebercarioPage } from './app.po';

describe('parquebercario App', () => {
  let page: ParquebercarioPage;

  beforeEach(() => {
    page = new ParquebercarioPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
